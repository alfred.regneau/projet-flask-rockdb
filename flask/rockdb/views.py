from .app import *
from flask import render_template, redirect, url_for, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import *


@app.route("/")
def home():
    return render_template(
        "home.html", title="Accueil"
    )

class LoginForm(FlaskForm):
    username = StringField('Utilisateur', validators=[validators.DataRequired()])
    password = PasswordField('Password', validators=[
                             validators.DataRequired()])
    submit = SubmitField('Se connecter')

    def get_authenticated_user(self):
        user = Utilisateur.query.get(self.username.data)
        if user is None:
            return None
        return user if user.check_password(self.password.data) else None


@app.route("/login", methods=("GET", "POST"))
def login():
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            return redirect(url_for("home"))
    return render_template(
        "login.html", form=f, title="Se connecter")


#==========PARTIE ARTISTE==========#

#Liste des artistes
@app.route("/artists/")
@app.route("/artists/<int:page>")
def artists(page=1):
    artists = get_all_artists()
    pages = len(artists)//40
    return render_template(
        "artists.html", title="Artistes", artists=artists, page=page, pages=pages)

#Listes des albums à partir d'une recherche
@app.route("/artists/", methods=["POST"])
@app.route("/artists/<int:page>", methods=["POST"])
def artists_post(page=1):
    artists = []
    check = request.form["search_option"]
    search = request.form["search"]
    if check == "artist":
        artists = get_artist_by_search_name(search)
    pages = len(artists)//40
    return render_template("artists.html", title="Artistes", artists=artists, page=page, pages=pages)


#Détail d'un artiste
@app.route("/detail/artist/<int:artistid>")
def artist(artistid):
    a = get_artist(artistid)
    return render_template(
        "artist.html",
        title="Artiste",
        artist=a)

class ArtistForm(FlaskForm):
    id = HiddenField('id')

    name = StringField('Nom',
                       [validators.InputRequired(),
                        validators.Length(min=2, max=100,
                                          message="Le nom doit avoir entre 2 et 100 caractères !"),

                        validators.Regexp('^[0-9a-zA-Z\-. ]+$',
                                          message="Le nom doit contenir seulement des lettres, des chiffres, des espaces ou des tirets")])

#Ajout/Edition d'un artiste
@app.route("/edit/artist/")
@app.route("/edit/artist/<int:artistid>")
def edit_artist(artistid=None):
    name = None
    if artistid is not None:  # je suis bien en modification
        a = get_artist(artistid)
        name = a.name
    else:
        a = None
    f = ArtistForm(id=artistid, name=name)
    return render_template(
        "edit_artist.html", artist=a, form=f)

#Sauvegarde d'un artistes (édité ou ajouté)
@app.route("/save/artist/", methods=["POST"])
def save_artist():
    f = ArtistForm()
    # Si on est en update d'Artist
    if f.id.data != "":
        id = int(f.id.data)
        a = get_artist(id)
    else:  # Sinon on crée un nouvel auteur
        a = Artist(name=f.name.data)
        db.session.add(a)
    if f.validate_on_submit():
        a.name = f.name.data
        db.session.commit()  # a aura un id attribué à présent
        id = a.id
        return redirect(url_for('artist', artistid=id))
    return render_template(
        "edit_artist.html",
        artist=a, form=f)


#Suppression dun artiste
@app.route("/delete/artist/<int:artistid>")
def delete_artist(artistid):
    artist = get_artist(artistid)
    for album in artist.albums.all():
        db.session.delete(album)
    db.session.delete(artist)
    db.session.commit()
    return redirect(url_for('artists'))

#==========PARTIE ALBUM==========#


#Listes des albums
@app.route("/albums/", methods=["GET"])
@app.route("/albums/<int:page>", methods=["GET"])

def albums(page=1):
    albums = get_all_albums()
    pages = len(albums)//40
    return render_template(
        "albums.html", title="Albums", albums=albums, page=page, pages=pages)

#Listes des albums à partir d'une recherche
@app.route("/albums/", methods=["POST"])
@app.route("/albums/<int:page>", methods=["POST"])
def albums_post(page=1):
    albums = []
    check = request.form["search_option"]
    search = request.form["search"]

    if check == "album":
        albums = get_album_by_title(search)

    elif check == "artist":
        albums = get_album_by_artist_name(search)

    elif check == "year":
        albums = get_album_by_year(search)

    elif check == "genre":
        albums = get_album_by_genre(search)
        
    pages = len(albums)//40
    return render_template("albums.html", title="Albums", albums=albums, page=page, pages=pages)


#Détail d'un album
@app.route("/detail/album/<int:albumid>")
def album(albumid):
    album = get_album(albumid)
    return render_template("album.html",
                           title="Album : " + album.title,
                           album=album)



class AlbumForm(FlaskForm):
    id = HiddenField('id')
    title = StringField('Titre',
                       [validators.InputRequired(),
                        validators.Length(min=2, max=150, message="Le nom doit avoir entre 2 et 150 caractères !"),
                        validators.Regexp('^[0-9a-zA-Z\-. ]+$',
                                          message="Le titre doit contenir seulement des lettres, des chiffres, des espaces ou des tirets")])

    img = StringField('Image', [validators.InputRequired(),
                                validators.Length(min=2, max=150, message="Vous devez saisir le lien vers l'image !")])

    release_year = StringField('Année',
                       [validators.InputRequired(),
                        validators.Length(min=4, max=4, message="Vous devez saisir une année correct !")])

    artist = StringField('Artiste', [validators.InputRequired(),
                                        validators.Length(min=1, max=10, message="Vous devez saisir l'identifiant d'un artiste !")])

    parent = StringField('Parent', [validators.InputRequired(),
                                        validators.Length(min=1, max=10, message="Vous devez saisir l'identifiant d'un artiste (parent) !")])



#Ajout/Edition d'un album
@app.route("/edit/album/")
@app.route("/edit/album/<int:albumid>")
def edit_album(albumid=None):
    if albumid is not None:  # je suis bien en modification
        a = get_album(albumid)
        title = a.title
        img = a.img
        release_year = a.release_year
        artist = a.artist.name
        parent = a.parent.name
    else:
        a = None
        title = None
        img = None
        release_year = None
        artist = None
        parent = None
    f = AlbumForm(id=albumid,
                  title=title,
                  img=img,
                  release_year=release_year,
                  artist=artist,
                  parent=parent)
    return render_template(
        "edit_album.html", album=a, form=f)


#Sauvegarde d'un album (édité ou ajouté)
@app.route("/save/album/", methods=["POST"])
def save_album():
    f = AlbumForm()
    # Si on est en update d'Album
    if f.id.data != "":
        id = int(f.id.data)
        a = get_album(id)
    else:  # Sinon on crée un nouvel album
        artist_id = get_artist_id(f.artist.data)
        parent_id = get_artist_id(f.parent.data)
        a = Album(title=f.title.data,
                  img=f.img.data,
                  release_year=int(f.release_year.data),
                  artist_id=artist_id,
                  parent_id=parent_id)
        db.session.add(a)
    if f.validate_on_submit():

        artist_id = get_artist_id(f.artist.data)
        parent_id = get_artist_id(f.parent.data)

        a.title = f.title.data
        a.img = f.img.data
        a.release_year = int(f.release_year.data)
        a.artist_id = artist_id
        a.parent_id = parent_id
        db.session.commit()  # a aura un id attribué à présent
        id = a.id
        return redirect(url_for('album', albumid=id))
    return render_template(
        "edit_album.html",
        album=a, form=f)

@app.route("/delete/album/<int:albumid>")
def delete_album(albumid):
    album = get_album(albumid)
    db.session.delete(album)
    db.session.commit()
    return redirect(url_for('albums'))


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html', title='Erreur 404'), 404

def get_artist_id(art_name):
    """
    Créé l'artiste si il n'existe pas et retourne son id
    """
    artist = get_artist_by_name(art_name)
    #Si l'artiste existe, on le récupère
    if artist.all():
        artist_id = artist.one().id
    #Sinon on le créé
    else:
        art = Artist(name=art_name)
        db.session.add(art)
        db.session.commit()
        artist_id = art.id
    return artist_id

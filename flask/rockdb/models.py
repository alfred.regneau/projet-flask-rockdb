from .app import db
from werkzeug.security import generate_password_hash, check_password_hash

"""
Ce fichier contient toutes les classes et requetes de la base de données
"""
class Utilisateur(db.Model):
    """
    Classe Utilisateur
    """
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(250), unique=True)
    mdp = db.Column(db.String(100))

    def set_password(self, password):
        self.mdp = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.mdp, password)
    def __repr__(self):
        return "Utilisateur : "+"(%d) %s" % (self.id, self.nom)

class Artist(db.Model):
    """
    Classe Auteur
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), unique=True)

    def __repr__(self):
        return "<Artist(%d) %s>" % (self.id, self.name)


class Album(db.Model):
    """
    Classe Album
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250))
    img = db.Column(db.String(250))
    release_year = db.Column(db.Integer())

    artist_id = db.Column(db.Integer, db.ForeignKey("artist.id"), nullable=False)
    artist = db.relationship("Artist", foreign_keys=[artist_id],
                             backref=db.backref("albums", lazy="dynamic"))

    parent_id = db.Column(db.Integer, db.ForeignKey("artist.id"))
    # parent = db.relationship("Artist", back_populates="Album")
    parent = db.relationship("Artist", foreign_keys=[parent_id],
                             backref=db.backref("albums_parent", lazy="dynamic"))


    def __repr__(self):
        return "<Album (%d) %s %s>" % (self.id, self.title, self.img)


class Genre(db.Model):
    """
    Classe Genre
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)

    def __repr__(self):
        return "<Genre(%d) %s>" % (self.id, self.name)

class Belong(db.Model):
    """
    Classe Belong (Appartenir)
    Permet d'associer un album et un genre
    """
    id = db.Column(db.Integer, primary_key=True)

    album_id = db.Column(db.Integer, db.ForeignKey("album.id"))
    album = db.relationship(Album, backref=db.backref("belong", cascade="all, delete-orphan"))

    genre_id = db.Column(db.Integer, db.ForeignKey("genre.id"))
    genre = db.relationship(Genre, backref=db.backref("belong", cascade="all, delete-orphan"))

    def __repr__(self):
        return "<Belong(%d) Album : %s Genre : %s>" % (self.id, self.album.title, self.genre.name)




def get_artist(id):
    """
    Donne l'artist correspondant à l'id
    """
    return Artist.query.get_or_404(id)

def get_album(id):
    """
    Donne l'album correspondant à l'id
    """
    return Album.query.get_or_404(id)

def get_artist_by_name(name):
    """
    Donne l'album correspondant au nom
    """
    return Artist.query.filter(Artist.name == name)

def get_artist_by_search_name(name):
    return Artist.query.filter(Artist.name.like("%" + name + "%")).all()



def get_all_artists():
    """
    Donne la liste de tous les artistes
    """
    return Artist.query.order_by(Artist.name).all()

def get_all_albums():
    """
    Donne la liste de tous les albums
    """
    return Album.query.order_by(Album.title).all()

def get_album_by_title(title):
    """
    Donne la liste de tous les albums contenant 'title' dans son titre
    """
    return Album.query.filter(Album.title.like("%"+title+"%")).order_by(Album.title).all()

def get_album_by_artist_name(name):
    """
    Donne la liste de tous les albums ayant dans leur nom d'artist 'name'
    """
    return Album.query.join(Album.artist).order_by(Album.title).filter(Artist.name.like("%"+name+"%")).all()

def get_album_by_year(year):
    """
    Donne la liste de tous les albums de l'année 'year'
    """
    try:
        albums = Album.query.filter(Album.release_year == int(year)).all()
    except ValueError:
        albums = []
    return albums

def get_album_by_genre(genre):
    """
    Donne la liste de tous les albums ayant dans leur genres 'genre'
    """
    return Album.query.join(Album.belong).join(Belong.genre).filter(Genre.name.like("%"+genre+"%")).all()